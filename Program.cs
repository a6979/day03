﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AdventOfCode3
{
	class Program
	{
		static void Main(string[] args)
		{
			var input = File.ReadAllLines("E:\\Programming\\csharp\\AdventOfCode\\3\\AdventOfCode3\\input.txt").ToList();

			var tempGammaElement = new List<string>();
			var tempEpsilonElement = new List<string>();
			var gamma = "";
			var epsilon = "";
			for (int i = 0; i < input[0].Length; i++)
			{
				foreach (var element in input)
				{
					var charArray = element.ToCharArray();
					var ch = charArray[i].ToString();
					tempGammaElement.Add(ch);
					tempEpsilonElement.Add(ch);
				}
				var zeroCount = tempGammaElement.Where(x => x.Equals("0")).Count();
				var oneCount = tempGammaElement.Where(x => x.Equals("1")).Count();
				var zeroEpsilonCount = tempEpsilonElement.Where(x => x.Equals("0")).Count();
				var oneEpsilonCount = tempEpsilonElement.Where(x => x.Equals("1")).Count();
				if (zeroCount > oneCount)
				{
					gamma += "0";
				}
				if (oneCount > zeroCount)
				{
					gamma += "1";
				}
				if (zeroEpsilonCount < oneEpsilonCount)
				{
					epsilon += "0";
				}
				if (oneEpsilonCount < zeroEpsilonCount)
				{
					epsilon += "1";
				}
				tempEpsilonElement.Clear();
				tempGammaElement.Clear();
			}

			Console.WriteLine("Gamma = " + gamma);
			Console.WriteLine("Epsilon = " + epsilon);

			var tempInput = input.ToList();

			var result = "";

			for (int i = 0; i < input[0].Length; i++)
			{
				var ones = tempInput.Where(x => x[i].ToString().Equals("1")).ToList();	
				var zeros = tempInput.Where(x => x[i].ToString().Equals("0")).ToList();
				if (ones.Count() > zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("1")).ToList();
				}
				if (ones.Count() < zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("0")).ToList();
				}
				if (ones.Count() == zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("1")).ToList();
				}
				if (tempInput.Count == 1)
				{
					result = tempInput[0].ToString();
					break;
				}
			}

			Console.WriteLine("oxygen = " + result);
			tempInput = input;
			for (int i = 0; i < input[0].Length; i++)
			{
				var ones = tempInput.Where(x => x[i].ToString().Equals("1")).ToList();
				var zeros = tempInput.Where(x => x[i].ToString().Equals("0")).ToList();
				if (ones.Count() > zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("0")).ToList();
				}
				if (ones.Count() < zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("1")).ToList();
				}
				if (ones.Count() == zeros.Count())
				{
					tempInput = tempInput.Where(x => x[i].ToString().Equals("0")).ToList();
				}
				if (tempInput.Count == 1)
				{
					result = tempInput[0].ToString();
					break;
				}
			}

			Console.WriteLine("CO2 scrubber = " + result);
			Console.ReadKey();
		}
	}
}